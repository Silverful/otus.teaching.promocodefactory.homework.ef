﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee()
            {
                Id = Guid.Parse("9ec93f7d-fe9f-43e0-a697-febd904e39c2"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                Role = Roles.FirstOrDefault(x => x.Name == "Admin"),
                AppliedPromocodesCount = 5
            },
            new Employee()
            {
                Id = Guid.Parse("16e10c6f-a034-4591-9480-c00df2170394"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                Role = Roles.FirstOrDefault(x => x.Name == "PartnerManager"),
                AppliedPromocodesCount = 10
            },
        };

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id = Guid.NewGuid(),
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = Guid.NewGuid(),
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };

        public static IEnumerable<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = Guid.NewGuid(),
                Name = "Театр",
            },
            new Preference()
            {
                Id = Guid.NewGuid(),
                Name = "Семья",
            },
            new Preference()
            {
                Id = Guid.Parse("13aa8224-3a45-4426-a914-04a855f46ebe"),
                Name = "Дети",
            }
        };

        public static IEnumerable<Customer> Customers
        {
            get
            {
                var id = Guid.NewGuid();

                var customers = new List<Customer>()
                {
                    new Customer()
                    {
                        Id = id,
                        Email = "ivan_sergeev@mail.ru",
                        FirstName = "Иван",
                        LastName = "Петров",
                        CustomerPreferences = new List<CustomerPreference>
                        {
                            new CustomerPreference
                            {
                                CustomerId = id,
                                PreferenceId=Preferences.First(x=>x.Name=="Театр").Id,
                            },
                            new CustomerPreference
                            {
                                CustomerId = id,
                                PreferenceId=Preferences.First(x=>x.Name=="Семья").Id,
                            }
                        }
                    }
                };

                return customers;
            }
        }

        public static IEnumerable<PromoCode> PromoCodes
        {
            get
            {
                return new List<PromoCode>()
                {
                    new PromoCode
                    {
                        Id = Guid.NewGuid(),
                        BeginDate = DateTime.Now.AddMonths(-1),
                        EndDate = DateTime.Now.AddMonths(1),
                        Code = "PROMO_TEST",
                        PartnerName = "Test Partner",
                        ServiceInfo = "Some service info",
                        PartnerManagerId = Guid.Parse("16e10c6f-a034-4591-9480-c00df2170394"),
                        PreferenceId = Guid.Parse("13aa8224-3a45-4426-a914-04a855f46ebe"),

                    }
                };
            }
        }
    }
}
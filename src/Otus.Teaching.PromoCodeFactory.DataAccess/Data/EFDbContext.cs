﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EFDbContext : DbContext
    {
        public EFDbContext(DbContextOptions options) : base(options) {}

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<PromoCode> PromoCodes { get; set; }
        public DbSet<CustomerPreference> CustomerPreferences { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>();
            modelBuilder.Entity<Role>();
            modelBuilder.Entity<Customer>().HasMany(c => c.CustomerPreferences);
            modelBuilder.Entity<Preference>();
            modelBuilder.Entity<PromoCode>();
            modelBuilder.Entity<CustomerPreference>().HasKey(x => new { x.CustomerId, x.PreferenceId });

            base.OnModelCreating(modelBuilder);
        }
    }
}

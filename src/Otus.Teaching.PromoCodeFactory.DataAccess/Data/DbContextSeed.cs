﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DbContextSeed
    {
        private EFDbContext _context;

        public DbContextSeed(EFDbContext context)
        {
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
            _context = context;
        }

        public void Seed()
        {
            _context.AddRange(FakeDataFactory.Employees);
            _context.SaveChanges();
            _context.AddRange(FakeDataFactory.Preferences);
            _context.SaveChanges();
            _context.AddRange(FakeDataFactory.PromoCodes);
            _context.SaveChanges();
            _context.AddRange(FakeDataFactory.Roles);
            _context.SaveChanges();
            _context.AddRange(FakeDataFactory.Customers);
            _context.SaveChanges();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }

        Task<IEnumerable<T>> IRepository<T>.GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> CreateAsync(T entity)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(T Entity)
        {
            var updatedData = Data;
            updatedData.Remove(Entity);
            Data = updatedData;
            return Task.CompletedTask;
        }

        public Task UpdateAsync(T entity)
        {
            var updatedData = Data;
            var toUpdatedIndex = Data.FindIndex(e => e.Id == entity.Id);
            updatedData[toUpdatedIndex] = entity;
            Data = updatedData;
            return Task.CompletedTask;
        }

        public Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            throw new NotImplementedException();
        }
    }
}
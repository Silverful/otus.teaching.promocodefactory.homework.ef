﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PromoCode
        : BaseEntity
    {
        [MaxLength(30)]
        public string Code { get; set; }

        [MaxLength(250)]
        public string ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        [MaxLength(30)]
        public string PartnerName { get; set; }
        public Guid? PartnerManagerId { get; set; }
        
        [ForeignKey("PartnerManagerId")]
        public virtual Employee PartnerManager { get; set; }
        public Guid PreferenceId { get; set; }

        [ForeignKey("PreferenceId")]
        public virtual Preference Preference { get; set; }
        public virtual IList<Customer> Customers { get; set; } = new List<Customer>();
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";
        [MaxLength(150)]
        public string Email { get; set; }
        
        public Guid? PromoCodeId { get; set; }

        [ForeignKey("PromoCodeId")]
        public virtual PromoCode PromoCode { get; set; }

        public virtual List<CustomerPreference> CustomerPreferences { get; set; } = new List<CustomerPreference>();
    }
}
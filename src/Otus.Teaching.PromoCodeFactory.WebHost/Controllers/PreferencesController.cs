﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController : Controller
    {
        private readonly IRepository<Preference> _preferencesRepository;

        public PreferencesController(IRepository<Preference> preferenceRepository)
        {
            _preferencesRepository = preferenceRepository;
        }

        /// <summary>
        /// Get all the preferences
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> GetPreferencesAsync()
        {
            var preferences = await _preferencesRepository.GetAllAsync();

            return new JsonResult(preferences.Select(p => new PreferenceResponse
            {
                Id = p.Id,
                Name = p.Name
            }));
        }
    }
}

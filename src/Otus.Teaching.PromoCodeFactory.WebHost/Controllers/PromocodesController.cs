﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private IRepository<Customer> _customerRepository;
        private IRepository<Preference> _preferenceRepository;
        private IRepository<PromoCode> _promoCodeRepository;

        public PromocodesController(IRepository<Customer> customerRepository,
                                    IRepository<Preference> preferenceRepository,
                                    IRepository<PromoCode> promoCodeRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _promoCodeRepository = promoCodeRepository;
        }


        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promos = await _promoCodeRepository.GetAllAsync();
            return new JsonResult(promos.Select(p => new PromoCodeShortResponse
            {
                Id = p.Id,
                Code = p.Code,
                ServiceInfo = p.ServiceInfo,
                BeginDate = p.BeginDate.ToShortDateString(),
                EndDate = p.EndDate.ToShortDateString(),
                PartnerName = p.PartnerName
            }));
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            DateTime beginDate, endDate;

            DateTime.TryParse(request.BeginDate, out beginDate);
            DateTime.TryParse(request.EndDate, out endDate);

            if (beginDate == null || endDate == null)
            {
                throw new ArgumentException("Dates are not in correct format");
            };

            var pref = (await _preferenceRepository.GetAllAsync()).Where(p => p.Name == request.Preference).First();
            var customers = pref.CustomerPreferences.Select(cp => cp.Customer);

            var promoCode = await _promoCodeRepository.CreateAsync(new PromoCode
            {
                BeginDate = beginDate,
                EndDate = endDate,
                Preference = pref,
                ServiceInfo = request.ServiceInfo,
                PartnerName = request.PartnerName,
                Code = request.PromoCode
            });

            foreach (var customer in customers)
            {
                customer.PromoCodeId = promoCode.Id;
                await _customerRepository.UpdateAsync(customer);
            }

            return Ok();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить данные о покупателях
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = new List<CustomerShortResponse>(customers.Select(c => new CustomerShortResponse
            {
                Id = c.Id,
                FirstName = c.FirstName,
                LastName = c.LastName,
                Email = c.Email
            })).ToList();

            return new JsonResult(response);
        }

        /// <summary>
        /// Получить данные о покупателях по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            var promoCode = customer.PromoCode;

            var response = new CustomerResponse
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                PromoCodes = new List<PromoCodeShortResponse>(),
                Preferences = customer.CustomerPreferences.Select(x => new PreferenceResponse
                {
                    Name = x.Preference.Name
                }).ToList()
            };

            if (promoCode != null)
            {
                response.PromoCodes.Add(new PromoCodeShortResponse
                {
                    Id = promoCode.Id,
                    Code = promoCode.Code,
                    PartnerName = promoCode.PartnerName,
                    ServiceInfo = promoCode.ServiceInfo,
                    BeginDate = promoCode.BeginDate.ToShortDateString(),
                    EndDate = promoCode.EndDate.ToShortDateString()
                });
            }

            return new JsonResult(response);
        }
        
        /// <summary>
        /// Создать нового покупателя
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customer = await CreateOrEditCustomerAsync(request);

            return new JsonResult(customer);
        }
        
        /// <summary>
        /// Отредактировать покупателя
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync([FromQuery] Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null) return NotFound();

            customer.CustomerPreferences = new List<CustomerPreference>();

            await CreateOrEditCustomerAsync(request, customer);

            await _customerRepository.UpdateAsync(customer);

            return new JsonResult(customer);
        }

        [NonAction]
        public async Task<Customer> CreateOrEditCustomerAsync(CreateOrEditCustomerRequest request, Customer customer = null)
        {
            if (customer == null)
            {
                customer = new Customer();
            }

            customer.Email = request.Email;
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;

            var prefs = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            foreach (var pref in prefs)
            {
                customer.CustomerPreferences.Add(new CustomerPreference() { Preference = pref });
            }

            return customer;
        }

        /// <summary>
        /// Удалить покупателя
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete] 
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null) return NotFound();

            await _customerRepository.DeleteAsync(customer);

            return Ok();
        }
    }
}